#!/bin/bash
HR=$HOME/.homesick/repos
HS=$HR/homeshick
if [ ! -d $HS ]; then
    git clone https://github.com/andsens/homeshick.git $HS
fi
source $HS"/homeshick.sh"
if [ ! -d $HR/barebones ]; then
    homeshick clone git@gitlab.com:ashiiish/homeshick-barebones.git
else
    homeshick pull homeshick-barebones
fi
homeshick link homeshick-barebones
